# qssi-user 10 QKQ1.200628.002 V12.0.4.0.QJSINXM release-keys
- manufacturer: xiaomi
- platform: lito
- codename: gauguin
- flavor: qssi-user
- release: 10
- id: QKQ1.200628.002
- incremental: V12.0.4.0.QJSINXM
- tags: release-keys
- fingerprint: Redmi/gauguinin/gauguin:10/QKQ1.200628.002/V12.0.4.0.QJSINXM:user/release-keys
Xiaomi/gauguininpro/gauguininpro:10/QKQ1.200628.002/V12.0.4.0.QJSINXM:user/release-keys
Xiaomi/gauguinin/gauguinin:10/QKQ1.200628.002/V12.0.4.0.QJSINXM:user/release-keys
- is_ab: false
- brand: Redmi
- branch: qssi-user-10-QKQ1.200628.002-V12.0.4.0.QJSINXM-release-keys
- repo: redmi_gauguin_dump
